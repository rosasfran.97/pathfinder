#ifndef GRILLA_H
#define GRILLA_H
#include "Celda.h"

class Grilla{
   public:
      unsigned int fil, col, tamCel; //tam pixels x celda
      int tamVec;
      int inicial[2];
      int objetivo[2];
      Celda*** celdas;
      Vertex linea[2];//Linea que une inicial y objetivo

   public:
      Grilla(unsigned int fil, unsigned int col, unsigned int tamCel);
      void setVecinos(int x, int y);
      void pintarVecinos(int x, int y);
      void pintarLinea();
      void display(RenderWindow &window); //Dibuja
      void reset();
      void setEstado(int x, int y, Celda::Estado e);
      void setInicial(int x, int y);
      void setObjetivo(int x, int y);
      bool fueraDeRango(int x, int y);
};

#endif //GRILLA_H
